.phony: all clean native release

all:
	$(MAKE) -C src
	-rm Main
	-ln -s src/Main.d.byte Main

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test.js0 clean
	-rm Main

native:
	$(MAKE) -C src
	-rm Main
	-ln -s src/Main.native Main

release:
	$(MAKE) -C src release
	-rm Main
	-ln -s src/Main.native Main

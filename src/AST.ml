open QUICr.Common

type bop =
| BAdd
| BEq
| BNe
| BLt
| BLe
| BSubSet
| BUnion
| BInter

type uop =
| UNeg
| UChoose
| USingleton

open QUICr.Const
    

type block = command list
and command =
| IfCommand of position * expr * block * block
| WhileCommand of position * expr * block
| ForEachCommand of position * variable * expr * block
| AssignCommand of position * variable * expr
| AssertCommand of position * expr
| AssumeCommand of position * expr
| KillCommand of position * variable list
and expr =
| BinaryExpr of position * bop * expr * expr
| UnaryExpr of position * uop * expr
| ConstExpr of position * const
| VarExpr of position * variable
| EmptySetExpr of position
| CallExpr of position * variable * expr list
     
type program = {
  variables: QUICr.Types.t SMap.t;
  program: block;
  var_tmp: int ref;
}

let rec fmt_phis ff = function
  | [] -> ()
  | (res,v1,v2)::t ->
    Format.fprintf ff "@,%s = phi(%s,%s)" res v1 v2;
    fmt_phis ff t

let fmt_const ff = function
  | NumConst n ->
    Format.fprintf ff "%d" n

let rec fmt_block ff block =
  QUICr.Listx.fmt (fun ff -> Format.fprintf ff "@,") fmt_command ff block
and fmt_command ff = function
  | IfCommand(_, cond, thenb, elseb) ->
    Format.fprintf ff "@[<v 2>if(%a) {@,%a@]@,@[<v 2>} else {@,%a@]@,}" fmt_expr cond fmt_block thenb fmt_block elseb
  | ForEachCommand(_, var, expr, body) ->
    Format.fprintf ff "@[<v 2>for(%s in %a) {@,%a@]@,}"
      var fmt_expr expr fmt_block body
  | WhileCommand(_, cond, body) ->
    Format.fprintf ff "@[<v 2>while(%a) {@,%a@]@,}"
      fmt_expr cond fmt_block body
  | AssignCommand(_, var, e) ->
    Format.fprintf ff "%s = %a;" var fmt_expr e
  | AssertCommand(_, e) ->
    Format.fprintf ff "assert(%a);" fmt_expr e
  | AssumeCommand(_, e) ->
    Format.fprintf ff "assume(%a);" fmt_expr e
  | KillCommand(_, vl) ->
    Format.fprintf ff "kill(%a);" (QUICr.Listx.fmt_str_sep ", " Format.pp_print_string) vl
and fmt_expr ff = function
  | BinaryExpr(_, op, lhs, rhs) ->
    begin match op with
    | BSubSet -> Format.fprintf ff "%a - %a" fmt_expr lhs fmt_expr rhs
    | _ ->
      Format.fprintf ff "(%a %s %a)" fmt_expr lhs (match op with
      | BAdd -> "+"
      | BEq -> "=="
      | BNe -> "!="
      | BLt -> "<"
      | BLe -> "<="
      | BUnion -> "|"
      | BInter -> "&"
      | _ -> assert false) fmt_expr rhs
    end
  | UnaryExpr(_, op, e) ->
    begin match op with
    | UNeg -> Format.fprintf ff "(-%a)" fmt_expr e
    | UChoose -> Format.fprintf ff "choose(%a)" fmt_expr e
    | USingleton -> Format.fprintf ff "{%a}" fmt_expr e
    end
  | ConstExpr(_, c) ->
    Format.fprintf ff "%a" fmt_const c
  | EmptySetExpr _ ->
    Format.fprintf ff "{}"
  | VarExpr (_, v) ->
    Format.fprintf ff "%s" v
  | CallExpr (_, v, args) ->
    Format.fprintf ff "%s(%a)" v (QUICr.Listx.fmt_str_sep ", " fmt_expr) args
    
    
and fmt_types ff types =
  let types = QUICr.Types.TSet.elements types in
  QUICr.Listx.fmt_str_sep ", " QUICr.Types.fmt ff types

let fmt_decls ff map =
  SMap.fmt ~sep:(fun ff -> Format.fprintf ff "@,")
    (fun ff var -> Format.fprintf ff "var %s" var)
    (fun ff typ -> Format.fprintf ff "%a;" QUICr.Types.fmt typ) ff map

let fmt ff pgm =
  Format.fprintf ff "@[<v 0>";
  fmt_decls ff pgm.variables;
  Format.fprintf ff "@,";
  fmt_block ff pgm.program;
  Format.fprintf ff "@]"


let expr_pos = function
  | BinaryExpr(pos,_,_,_)
  | UnaryExpr(pos,_,_)
  | EmptySetExpr pos
  | ConstExpr(pos, _)
  | VarExpr (pos, _)
  | CallExpr (pos, _, _) ->
    pos

let fresh_var_i ci =
  let res = !ci in
  incr ci;
  "$"^(string_of_int res)

let fresh_var p =
  fresh_var_i p.var_tmp


open QUICr.Common
open AST
open QUICr.Commands

(* threaded state keeps track of current set of blocks

   It tracks out_blocks, which is a current set of completed blocks and a block
   that is currently under construction tracked by curr_*.

   A block is made of three parts in order:
   1. asserts
   2. guards (assumed)
   3. commands
   
*)

type threaded_state = {
  out_blocks: control_t list;
  curr_guards: guard_set_t;
  curr_commands: command_list_t;
  curr_asserts: guard_list_t;
  vars: QUICr.Types.t QUICr.Common.SMap.t;
  tvar_tmp: int ref;
}

let id_counter = ref 0

let fresh_id () =
  let id = !id_counter in
  incr id_counter;
  id

(* completes the block currently under construction (places curr in out_blocks).
   If block is empty, does nothing
*)
let complete_block state =
  if GSet.is_empty state.curr_guards &&
    state.curr_commands = [] &&
    state.curr_asserts = []
  then
    state
  else begin
    let block = {
      guards = state.curr_guards;
      commands = List.rev state.curr_commands;
      assertions = List.rev state.curr_asserts;
    } in
    {
      out_blocks = (BlockControl (block, fresh_id()))::state.out_blocks;
      curr_guards = GSet.empty;
      curr_commands = [];
      curr_asserts = [];
      vars = state.vars;
      tvar_tmp = state.tvar_tmp;
    }
  end
  
(* adds a guard to the current block.  Creates a new block as needed *)
let add_guard state guard =
  let state = if state.curr_commands <> [] then
      complete_block state
    else
      state
  in
  { state with
    curr_guards = GSet.add guard state.curr_guards;
  }


(* adds an assert to the current block.  Creates a new block as needed *)
let add_assert state guard =
  let state = if state.curr_commands <> [] ||
      not (GSet.is_empty state.curr_guards) then
      complete_block state
    else
      state
  in
  { state with
    curr_asserts = guard :: state.curr_asserts;
  }

(* adds a command to the current block.  Creates a new block as needed *)
let add_command state cmd =
  { state with
    curr_commands = cmd::state.curr_commands;
  }

(* gets the final result from out_blocks.  Completes a current block if necessary. *)
let flush_state state =
  let state = complete_block state in
  let out_blocks = state.out_blocks in
  match out_blocks with
  | [block] -> block
  | _ ->
    SeqControl (List.rev out_blocks)


(* adds an if block to the out_blocks.  Everything should be already completed. *)
let add_if state thenb elseb =
  { state with
    out_blocks = (IfControl (thenb, elseb, fresh_id())) :: state.out_blocks;
  }

(* adds a while block to the out_blocks.  Everything should be already completed. *)
let add_while state body =
  { state with
    out_blocks = (WhileControl (fresh_id(), body, fresh_id())) :: state.out_blocks;
  }


(* gets a fresh threaded state *)
let new_state tvar_tmp vars = {
  out_blocks = [];
  curr_guards = GSet.empty;
  curr_commands = [];
  curr_asserts = [];
  vars = vars;
  tvar_tmp = tvar_tmp;
}




let rec convert_block state = function
  | [] -> state
  | cmd::rest ->
    let state = convert_command state cmd in
    convert_block state rest

and convert_command state = function
  | AST.AssumeCommand(position, e) ->
    let guard = convert_bool state position e in
    add_guard state guard

  | AST.AssertCommand(position, e) ->
    let guard = convert_bool state position e in
    let state = add_assert state guard in
    add_guard state guard

  | AST.KillCommand(position, vars) ->
    let num_vars = ref [] in
    let set_vars = ref [] in
    List.iter (fun v ->
      try
        match QUICr.Common.SMap.find v state.vars with
        | QUICr.Types.Set -> set_vars := v::!set_vars
        | QUICr.Types.Number -> num_vars := v::!num_vars
        | _ -> assert false
      with Not_found ->
        assert false
    ) vars;
    let state = if !num_vars <> [] then
        add_command state (KillNumCommand, position, !num_vars)
      else state in
    let state = if !set_vars <> [] then
        add_command state (KillSetCommand, position, !set_vars)
      else state in
    state

  | AST.AssignCommand(position, lhs, e) ->
    convert_assign state position lhs e

  | AST.IfCommand(position, cond, thenb, elseb) ->
    convert_if state position cond thenb elseb

  | AST.WhileCommand(position, cond, body) ->
    convert_while state position cond body

  | AST.ForEachCommand(position, var, VarExpr(pos,expr), body) ->
    convert_for state position var expr body
  | AST.ForEachCommand(position, var, _, body) ->
    assert false

and convert_bool state position = function
  | BinaryExpr((spos,epos), op, VarExpr(posa, a), VarExpr(posb, b)) ->
    let resop = match (QUICr.Common.SMap.find a state.vars, op) with
      | (QUICr.Types.Number, BEq) -> EqualGuard
      | (QUICr.Types.Number, BNe) -> NotEqualGuard
      | (QUICr.Types.Number, BLt) -> LessThanGuard
      | (QUICr.Types.Number, BLe) -> LessEqGuard
      | (QUICr.Types.Set, BEq) -> SetEqGuard
      | (QUICr.Types.Set, BNe) -> raise (QUICr.Common.Parse_error (spos, epos, "Cannot declare sets not equal"))
      | (QUICr.Types.Set, BLt) -> SubSetEqGuard
      | (QUICr.Types.Set, BLe) -> SubSetEqGuard
      | _ -> assert false
    in
    (resop, (spos,epos), [a; b])
  | _ -> assert false

and negate_guard (guard_name, position, arguments) =
  match guard_name with
  | SubSetEqGuard -> Some (SubSetEqGuard, position, List.rev arguments)
  | SetEqGuard -> None
  | LessThanGuard -> Some (LessEqGuard, position, List.rev arguments)
  | LessEqGuard -> Some (LessThanGuard, position, List.rev arguments)
  | EqualGuard -> Some (NotEqualGuard, position, arguments)
  | NotEqualGuard -> Some (EqualGuard, position, arguments)

and convert_assign state position lhs e =
  let cmd = match e with
    | BinaryExpr(pos, op, VarExpr(posa, a), VarExpr(posb, b)) ->
      let resop = match op with
        | BAdd -> AddCommand
        | BSubSet -> SubSetCommand
        | BUnion -> UnionSetCommand
        | BInter -> InterSetCommand
        | _ -> assert false
      in
      (resop, pos, [lhs; a; b])
    | UnaryExpr(pos, op, VarExpr(pose, e)) ->
      let resop = match op with
        | UNeg -> NegCommand
        | UChoose -> ChooseCommand
        | USingleton -> SingletonSetCommand
      in
      (resop, pos, [lhs; e])
    | ConstExpr(pos, c) ->
      (ConstCommand c, pos, [lhs])
    | VarExpr(pose, v) ->
      begin
        try
          match QUICr.Common.SMap.find v state.vars with
          | QUICr.Types.Set ->
            (AssignSetCommand, pose, [lhs; v])
          | QUICr.Types.Number ->
            (AssignNumCommand, pose, [lhs; v])
          | _ -> assert false
        with Not_found ->
          assert false
      end
    | EmptySetExpr(pose) ->
      (EmptySetCommand, pose, [lhs])
    | _ ->
      Format.printf "Error: %a@." AST.fmt_expr e;
      assert false
  in
  add_command state cmd

and convert_if state position cond thenb elseb =
  let state = complete_block state in
  let guard = convert_bool state position cond in
  let then_state = add_guard (new_state state.tvar_tmp state.vars) guard in
  let then_state = convert_block then_state thenb in

  let else_state = match negate_guard guard with
    | Some g ->
      add_guard (new_state state.tvar_tmp state.vars) g
    | None ->
      new_state state.tvar_tmp state.vars
  in
  let else_state = convert_block else_state elseb in

  let then_state = flush_state then_state in
  let else_state = flush_state else_state in
  add_if state then_state else_state

and convert_while state position cond body =
  let guard = convert_bool state position cond in

  let body_state = add_guard (new_state state.tvar_tmp state.vars) guard in
  let body_state = convert_block body_state body in
  let state = {state with
    vars = body_state.vars
  } in

  let state = complete_block state in
  let body_state = flush_state body_state in
  let state = add_while state body_state in
  match negate_guard guard with
  | None -> state
  | Some g ->
    add_guard state g

and convert_for state position x s body =
  (* generate fresh variables *)
  let yi = AST.fresh_var_i state.tvar_tmp in
  let yo = AST.fresh_var_i state.tvar_tmp in
  let xs = AST.fresh_var_i state.tvar_tmp in
  let state = { state with
    vars = SMap.add yi QUICr.Types.Set state.vars } in
  let state = { state with
    vars = SMap.add yo QUICr.Types.Set state.vars } in
  let state = { state with
    vars = SMap.add xs QUICr.Types.Set state.vars } in


  (* add preamble *)
  let state = add_command state (EmptySetCommand, position, [yo]) in (* yo = {} *)
  let state = add_command state (AssignSetCommand, position, [yi; s]) in (* yi = s *)
  let state = add_command state (UnionSetCommand, position, [s; yo; yi]) in (* s = yi u yo *)
  let state = add_guard state (SetEqGuard, position, [yi; s]) in (* assume(yi = s) *)
  let state = add_guard state (SubSetEqGuard, position, [yo; s]) in (* assume(yo c= s) *)

  (* generate body *)
  let body_state = new_state state.tvar_tmp state.vars in
  let body_state = add_command body_state (ChooseCommand, position, [x; yi]) in
  let body_state = add_guard body_state (SetEqGuard, position, [yi; yi]) in
  let body_state = add_command body_state (SingletonSetCommand, position, [xs; x]) in
  let body_state = add_guard body_state (SetEqGuard, position, [yi; yi]) in
  let body_state = add_command body_state (SubSetCommand, position, [yi; yi; xs]) in
  let body_state = add_guard body_state (SetEqGuard, position, [yi; yi]) in
  let body_state = convert_block body_state body in
  let body_state = add_command body_state (UnionSetCommand, position, [yo; yo; xs]) in
  let body_state = add_command body_state (KillSetCommand, position, [xs]) in
  let body_state = add_command body_state (KillNumCommand, position, [x]) in
  let state = {state with
    vars = body_state.vars;
  } in
  let body_state = flush_state body_state in

  (* add while *)
  let state = complete_block state in
  let state = add_while state body_state in

  (* add post state *)
  let state = add_command state (EmptySetCommand, position, [xs]) in
  let state = add_guard state (SetEqGuard, position, [yi; xs]) in
  let state = add_guard state (SetEqGuard, position, [yo; s]) in
  let state = add_command state (KillSetCommand, position, [yi; yo; xs]) in
  state

  
let convert program =
  id_counter := 0;
  let init_id = fresh_id() in
  let final_state = convert_block (new_state program.AST.var_tmp program.AST.variables) program.AST.program in
  {
    init_id = init_id;
    program = flush_state final_state;
    variables = final_state.vars;
    var_tmp = program.AST.var_tmp;
  }

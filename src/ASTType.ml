open QUICr.Common
open QUICr.Types
open QUICr.Const
open AST

let number_t = TSet.singleton Number
let set_t = TSet.singleton Set
let num_set_t = TSet.add Number set_t

let type_error e msg =
  let (spos, epos) = expr_pos e in
  raise (Parse_error (spos, epos, msg))

let rec expr_type vars = function
  | BinaryExpr(_, op, lhs, rhs) ->
    begin match op with
    | BAdd ->
      let lhs_t = expr_type vars lhs in
      let rhs_t = expr_type vars rhs in
      if lhs_t <> Number then
        type_error lhs "Expected numeric argument";
      if rhs_t <> Number then
        type_error rhs "Expected numeric argument";
      Number
    | BEq
    | BNe
    | BLt
    | BLe ->
      let lhs_t = expr_type vars lhs in
      let rhs_t = expr_type vars rhs in
      if lhs_t <> rhs_t then
        type_error rhs ("Expected "^(toString lhs_t)^" type");
      Bool
    | BUnion
    | BSubSet
    | BInter ->
      let lhs_t = expr_type vars lhs in
      let rhs_t = expr_type vars rhs in
      if lhs_t <> Set then
        type_error lhs "Expected set argument";
      if rhs_t <> Set then
        type_error rhs "Expected set argument";
      Set
    end
  | UnaryExpr(_, op, e) ->
    begin match op with
    | UNeg ->
      let e_t = expr_type vars e in
      if e_t <> Number then
        type_error e "Expected numeric argument";
      Number
    | UChoose ->
      let e_t = expr_type vars e in
      if e_t <> Set then
        type_error e "Expected set argument";
      Number
    | USingleton ->
        let e_t = expr_type vars e in
        if e_t <> Number then
          type_error e "Expected numeric argument";
        Set
    end
  | ConstExpr(_, NumConst _) ->
    Number
  | VarExpr(_ , v) as e ->
    begin
      try
        SMap.find v vars
      with Not_found ->
        type_error e ("Variable '"^v^"' is not declared")
    end
  | EmptySetExpr _ ->
    Set
  | CallExpr(_,_,_) as e ->
    type_error e "Calls to unknown functions are unsupported"

    

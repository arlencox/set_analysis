module Commands = QUICr.Commands
let print_raw = ref false
let wrap_amount = ref 5

let args = [
  ("-apron-print-raw", Arg.Set print_raw, "Print using Apron's internal pretty printer");
  ("-apron-wrap-amount", Arg.Set print_raw, "Number of conuncts before line breaking")
]

module type ApronSignature = sig
  type t
  val alloc_manager: unit -> t Apron.Manager.t
end

module ApronDomain(S:ApronSignature) : QUICr.Domain.ScalarDomain = struct
  open FormatHelp
  (* This is a basic domain for numbers and dictionaries:
     A dictionary is represented as a pair of symbolic variables called key and value.
     The key is the set of all keys that are mapped in the dictionary and value is the set
     of all values mapped to by those keys.  This is strictly a weak-updates domain. *)
    
  type t = S.t Apron.Abstract0.t

  type ctx = S.t Apron.Manager.t


  let args = []

  let init () = S.alloc_manager ()

  let dim_size ctx t =
    (Apron.Abstract0.dimension ctx t).Apron.Dim.intd

  let get_dim = dim_size

  let top ctx dim =
    Apron.Abstract0.top ctx dim 0

  let bottom ctx dim =
    Apron.Abstract0.bottom ctx dim 0

  let meet ctx a b =
    assert((dim_size ctx a) = (dim_size ctx b));
    Apron.Abstract0.meet ctx a b

  let join ctx a b =
    assert((dim_size ctx a) = (dim_size ctx b));
    Apron.Abstract0.join ctx a b

  let widen ctx a b =
    assert((dim_size ctx a) = (dim_size ctx b));
    Apron.Abstract0.widening ctx a b

  let narrow _ctx _a b =
    b

  let is_bottom ctx state =
    Apron.Abstract0.is_bottom ctx state

  let is_top ctx state =
    Apron.Abstract0.is_top ctx state


  let add_dim ctx t =
    let prev_d = (Apron.Abstract0.dimension ctx t).Apron.Dim.intd in
    Apron.Abstract0.add_dimensions
      ctx
      t
      {Apron.Dim.dim = [|prev_d|];
       Apron.Dim.intdim=1;
       Apron.Dim.realdim=0} false


  let rem_dim ctx t =
    let prev_d = (Apron.Abstract0.dimension ctx t).Apron.Dim.intd in
    Apron.Abstract0.remove_dimensions
      ctx
      t
      {Apron.Dim.dim = [|prev_d-1|];
       Apron.Dim.intdim=1;
       Apron.Dim.realdim=0}


  let transition ctx start_state (cmd, pos, args) =
    match cmd with
    | Commands.AddCommand ->
      begin match args with
      | [] -> assert false
      | lhs::args ->
        let rhs = List.fold_left (fun sum el ->
          Apron.Texpr0.binop Apron.Texpr0.Add Apron.Texpr0.Int Apron.Texpr0.Near sum (Apron.Texpr0.dim el)
        ) (Apron.Texpr0.cst (Apron.Coeff.s_of_int 0)) args in
        Apron.Abstract0.assign_texpr_array ctx start_state [| lhs |] [| rhs |] None
      end
    | Commands.NegCommand ->
      begin match args with
      | [lhs; rhs] ->
          let rhs = Apron.Linexpr0.of_list None [(Apron.Coeff.s_of_int (-1), rhs)] None in
          Apron.Abstract0.assign_linexpr_array ctx start_state [| lhs |] [| rhs |] None
      | _ -> assert false
      end
    | Commands.AssignNumCommand ->
      begin match args with
      | [lhs; rhs] ->
        let rhs = Apron.Texpr0.dim rhs in
        Apron.Abstract0.assign_texpr_array ctx start_state [| lhs |] [| rhs |] None
      | _ -> assert false
      end
    | Commands.ConstCommand (QUICr.Const.NumConst c) ->
      begin match args with
      | [lhs] ->
        let rhs = Apron.Texpr0.cst (Apron.Coeff.s_of_int c) in
        Apron.Abstract0.assign_texpr_array ctx start_state [| lhs |] [| rhs |] None
      | _ -> assert false
      end
    | Commands.KillNumCommand ->
      let lhs = Array.of_list args in
      let rhsi = Apron.Texpr0.cst (Apron.Coeff.Interval Apron.Interval.top) in
      let rhs = Array.make (Array.length lhs) rhsi in
      Apron.Abstract0.assign_texpr_array ctx start_state lhs rhs None
    | _ -> assert false
    
  let constrain ctx start_state (guard, pos, args) =
    let (a, b) = match args with
      | [a; b] -> (a,b)
      | _ -> assert false
    in
    let a = Apron.Texpr0.dim a in
    let b = Apron.Texpr0.dim b in
    let diff = Apron.Texpr0.binop Apron.Texpr0.Sub Apron.Texpr0.Int Apron.Texpr0.Near b a in
    let typ = match guard with
    | Commands.LessThanGuard ->
      Apron.Tcons0.SUP
    | Commands.LessEqGuard ->
      Apron.Tcons0.SUPEQ
    | Commands.EqualGuard ->
      Apron.Tcons0.EQ
    | Commands.NotEqualGuard ->
      Apron.Tcons0.DISEQ
    | _ -> assert false
    in
    let tcons = Apron.Tcons0.make diff typ in
    Apron.Abstract0.meet_tcons_array ctx start_state [| tcons |]


  (************ formatting *************)

  let fmt_typ escape ff = function
    | Apron.Lincons0.EQ -> fmt_eq escape ff ()
    | Apron.Lincons0.SUPEQ -> fmt_ge escape ff ()
    | Apron.Lincons0.SUP -> fmt_gt escape ff ()
    | Apron.Lincons0.DISEQ -> fmt_ne escape ff ()
    | Apron.Lincons0.EQMOD _ -> assert false

  let fmt_linexpr escape fmt_var ff le =
    let printed = ref false in
    Apron.Linexpr0.iter (fun coeff dim ->
      match coeff with
      | Apron.Coeff.Scalar s ->
        let ss = Apron.Scalar.sgn s in
        if ss = 0 then
          () (* don't do anything *)
        else if ss > 0 then begin
          if !printed then
            Format.fprintf ff " %a " (fmt_plus escape) ();
          printed := true;
          Format.fprintf ff "%a%a%a" Apron.Scalar.print s (fmt_mul escape) () fmt_var dim
        end
        else begin
          if !printed then begin
            let ps = Apron.Scalar.neg s in
            Format.fprintf ff "%a %a%a%a" (fmt_minus escape) () Apron.Scalar.print ps (fmt_mul escape) () fmt_var dim
          end
          else begin
            printed := true;
            Format.fprintf ff "%a%a%a" Apron.Scalar.print s (fmt_mul escape) () fmt_var dim
          end
        end
          
      | Apron.Coeff.Interval i ->
        assert false
    ) le;
    Format.fprintf ff " %a " (fmt_plus escape) ();
    Apron.Coeff.print ff (Apron.Linexpr0.get_cst le)

  type fmt_typ =
    | FT_Num of Apron.Coeff.t
    | FT_Var of Apron.Coeff.t * Apron.Dim.t

  let fmt_lincons escape fmt_var ff lc =
    let lhs = ref [] in
    let rhs = ref [] in
    (* log all of the variables *)
    Apron.Linexpr0.iter (fun coeff dim ->
      match coeff with
        | Apron.Coeff.Scalar s ->
            let ss = Apron.Scalar.sgn s in
            if ss = 0 then
              () (* zero coefficient, skip it *)
            else if ss > 0 then
              lhs := (FT_Var (coeff,dim)):: !lhs
            else
              let coeff = Apron.Coeff.neg coeff in
              rhs := (FT_Var (coeff,dim)):: !rhs
        | Apron.Coeff.Interval i ->
            assert false
    ) lc.Apron.Lincons0.linexpr0;
    (* log the constant *)
    let const = Apron.Linexpr0.get_cst lc.Apron.Lincons0.linexpr0 in
    begin match const with
      | Apron.Coeff.Scalar s ->
          let ss = Apron.Scalar.sgn s in
          if ss = 0 then
            () (* zero coefficient, skip it *)
          else if ss > 0 then
            lhs := (FT_Num const):: !lhs
          else
            let const = Apron.Coeff.neg const in
            rhs := (FT_Num const):: !rhs
      | Apron.Coeff.Interval i ->
          assert false
    end;
    let lhs = List.rev !lhs in
    let rhs = List.rev !rhs in

    (* print everything out *)

    let print_el ff el =
      match el with
        | FT_Num c -> Apron.Coeff.print ff c
        | FT_Var (c,v) ->
            if Apron.Coeff.equal_int c 1 then
              fmt_var ff v
            else begin
              Apron.Coeff.print ff c;
              Format.fprintf ff "%a" (fmt_mul escape) ();
              fmt_var ff v
            end
    in

    let print_list ff l =
      if l = [] then
        Format.fprintf ff "0"
      else
        QUICr.Listx.fmt
          (fun ff -> Format.fprintf ff " %a " (fmt_plus escape) ())
          print_el
          ff l
    in

    Format.fprintf ff "%a %a %a"
      print_list lhs
      (fmt_typ escape) lc.Apron.Lincons0.typ
      print_list rhs
      
    

  let fmt ?escape:(escape=Text) ctx fmt_var ff st =
    if !print_raw then
      let fmt_v id =
        let buf = Buffer.create 29 in
        let ffbuf = Format.formatter_of_buffer buf in
        fmt_var ffbuf id;
        Format.pp_print_flush ffbuf ();
        Buffer.contents buf
      in
      Apron.Abstract0.print fmt_v ff st
    else if Apron.Abstract0.is_bottom ctx st then
      fmt_bottom escape ff ()
    else if Apron.Abstract0.is_top ctx st then
      fmt_top escape ff ()
    else begin
      let tab = Apron.Abstract0.to_lincons_array ctx st in
      let first = ref true in
      Array.iteri (fun i tc ->
        if !first then
          first := false
        else begin
          Format.fprintf ff " %a " (fmt_conj escape) ();
          if (i mod !wrap_amount) = 0 && i <> 0 then
            FormatHelp.fmt_newline escape ff ()
        end;
        fmt_lincons escape fmt_var ff tc
      ) tab
    end

  (*let fmt ?escape:(escape=Text) ctx fmt_var ff st =
    let fmt_v id =
      let buf = Buffer.create 29 in
      let ffbuf = Format.formatter_of_buffer buf in
      fmt_var ffbuf id;
      Format.pp_print_flush ffbuf ();
      Buffer.contents buf
    in
    Apron.Abstract0.print fmt_v ff st*)

  exception Short_circuit

  type eq_type =
    | EQ_Coeff of Apron.Scalar.t
    | EQ_Dim of Apron.Dim.t

  let eq_cmp a b =
    match a,b with
      | EQ_Coeff a, EQ_Coeff b -> Apron.Scalar.cmp a b
      | EQ_Dim a, EQ_Dim b -> compare a b
      | EQ_Coeff _, EQ_Dim _ -> -1
      | EQ_Dim _, EQ_Coeff _ -> 1

  let get_eq_le ?pos:(pos=None) ?neg:(neg=None) add eq uf le =
    try
      let pos_dim = ref pos in
      let neg_dim = ref neg in
      Apron.Linexpr0.iter (fun coeff dim ->
        if Apron.Coeff.equal_int coeff 1 then
          if !pos_dim = None then
            pos_dim := Some (EQ_Dim dim)
          else
            raise Short_circuit
        else if Apron.Coeff.equal_int coeff (-1) then
          if !neg_dim = None then
            neg_dim := Some (EQ_Dim dim)
          else
            raise Short_circuit
      ) le;
      match !pos_dim, !neg_dim with
        | Some p, Some n ->
            let uf = add uf p in
            let uf = add uf n in
            let uf = eq uf p n in
            uf
        | _ -> uf
    with Short_circuit ->
      uf

  let get_eq_lc add eq uf lc =
    let const = Apron.Linexpr0.get_cst lc.Apron.Lincons0.linexpr0 in
    match const, lc.Apron.Lincons0.typ with
      | Apron.Coeff.Scalar s, Apron.Lincons0.EQ  ->
          let ss = Apron.Scalar.sgn s in
          if ss = 0 then begin
            (* search for variable equalities *)
            get_eq_le add eq uf lc.Apron.Lincons0.linexpr0
          end else begin
            if ss > 0 then
              (* search for neg variable eq pos const *)
              get_eq_le ~pos:(Some (EQ_Coeff s)) add eq uf lc.Apron.Lincons0.linexpr0
            else
              (* search for pos variable eq neg const *)
              get_eq_le ~neg:(Some (EQ_Coeff s)) add eq uf lc.Apron.Lincons0.linexpr0
          end
      | _ -> uf
    

  let filter_eqs l =
    QUICr.Listx.rev_map_filter (fun l ->
      let res = QUICr.Listx.rev_map_filter (function
        | EQ_Dim d -> Some d
        | EQ_Coeff _ -> None
      ) l in
      if res = [] then
        None
      else
        Some res
    ) l

  let get_eq ctx st =
    if Apron.Abstract0.is_bottom ctx st then
      []
    else if Apron.Abstract0.is_top ctx st then
      []
    else begin
      let module IO = struct
        type t = eq_type
        let compare = eq_cmp
      end in
      let module UF = UnionFind.Make(IO) in

      let tab = Apron.Abstract0.to_lincons_array ctx st in
      let uf = ref UF.empty in
      Array.iter (fun lc ->
        uf := get_eq_lc UF.add UF.union !uf lc
      ) tab;
      filter_eqs (UF.equivalence_classes !uf)
    end

  let le ctx a b =
    (*Format.printf "a: %a@." (fmt ctx (fun ff i -> Format.fprintf ff "b%d" i)) a;
    Format.printf "b: %a@." (fmt ctx (fun ff i -> Format.fprintf ff "b%d" i)) b;*)
    assert((dim_size ctx a) = (dim_size ctx b));
    Apron.Abstract0.is_leq ctx a b
end

module PolkaSig : ApronSignature = struct
  type t = Polka.loose Polka.t

  let alloc_manager () =
    Polka.manager_alloc_loose ()
end

module PolkaDomain = ApronDomain(PolkaSig)


module BoxSig : ApronSignature = struct
  type t = Box.t

  let alloc_manager () =
    Box.manager_alloc ()
end

module BoxDomain = ApronDomain(BoxSig)

module OctSig : ApronSignature = struct
  type t = Oct.t

  let alloc_manager () =
    Oct.manager_alloc ()
end

module OctDomain = ApronDomain(OctSig)

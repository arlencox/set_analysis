open QUICr.Common
open QUICr.Commands

let indent_amt = 2

let rec fmt_indent ff indent =
  if indent <> 0 then begin
    Format.fprintf ff "&nbsp;";
    fmt_indent ff (indent - 1)
  end

let fmt_annotation indent fmt_post_state ff id =
  Format.fprintf ff "%a<span class=\"annotation\">\\( \\displaystyle \\left\\{ \\begin{array}{l}%a\\end{array} \\right\\} \\)</span><br/>@," fmt_indent indent fmt_post_state id

let fmt_arg arg =
  (*try
    let dollar = String.rindex arg '$' in
    let base = String.sub arg 0 dollar in
    let sub = String.sub arg (dollar+1) ((String.length arg) - dollar - 1) in
    base ^ "<sub>" ^ sub ^ "</sub>"
  with Not_found ->*)
    arg

let rec fmt_control indent fmt_post_state ff cmds =
  let fmt_annot = fmt_annotation (indent + indent_amt) fmt_post_state ff in
  match cmds with
  | BlockControl (block, id) ->
    fmt_block indent fmt_post_state ff block;
    fmt_annot id
  | SeqControl (seq) ->
    fmt_seq indent fmt_post_state ff seq
  | IfControl(thenc, elsec, id) ->
    fmt_if indent fmt_post_state ff thenc elsec;
    fmt_annot id
  | WhileControl (inv_id, body, post_id) ->
    fmt_while indent fmt_post_state inv_id ff body;
    fmt_annot post_id

and fmt_seq indent fmt_post_state ff seq =
  List.iter (fmt_control indent fmt_post_state ff) seq

and fmt_if indent fmt_post_state ff thenc elsec =
  Format.fprintf ff "%a<span class=\"keyword\">if</span>&nbsp;(*)&nbsp;{<br/>@," fmt_indent indent;
  fmt_control (indent + indent_amt) fmt_post_state ff thenc;
  Format.fprintf ff "%a} <span class=\"keyword\">else</span> {<br/>@," fmt_indent indent;
  fmt_control (indent + indent_amt) fmt_post_state ff elsec;
  Format.fprintf ff "%a}<br/>@," fmt_indent indent

and fmt_while indent fmt_post_state inv_id ff body =
  Format.fprintf ff "%a<span class=\"keyword\">while</span>&nbsp;(*)<br/>@," fmt_indent indent;
  fmt_annotation (indent + indent_amt) fmt_post_state ff inv_id;
  Format.fprintf ff "%a{<br/>@," fmt_indent indent;
  fmt_control (indent + indent_amt) fmt_post_state ff body;
  Format.fprintf ff "%a}<br/>@," fmt_indent indent

and fmt_block indent fmt_post_state ff block =
  List.iter (fmt_guard true indent ff) block.assertions;
  GSet.iter (fmt_guard false indent ff) block.guards;
  List.iter (fmt_command indent ff) block.commands

and fmt_command indent ff (name, position, args) =
  Format.fprintf ff "%a" fmt_indent indent;
  let args = List.map fmt_arg args in
  begin match name, args with
  | (SubSetCommand, [result; set; index]) ->
    Format.fprintf ff "%s = %s - %s" result set index
  | (SubSetCommand, _ ) -> assert false
  | (AddCommand, result::args) ->
    Format.fprintf ff "%s = %a" result (QUICr.Listx.fmt_str_sep " + " (fun ff s -> Format.fprintf ff "%s" s)) args
  | (AddCommand, _ ) -> assert false
  | (NegCommand, [result; operand]) ->
    Format.fprintf ff "%s = -%s" result operand
  | (NegCommand, _ ) -> assert false
  | (AssignSetCommand, [result; operand]) ->
    Format.fprintf ff "%s = %s" result operand
  | (AssignNumCommand, [result; operand]) ->
    Format.fprintf ff "%s = %s" result operand
  | (AssignSetCommand, _ ) -> assert false
  | (AssignNumCommand, _ ) -> assert false
  | (ConstCommand (QUICr.Const.NumConst i), [result]) ->
    Format.fprintf ff "%s = <span class=\"number\">%d</span>" result i
  | (ConstCommand _, _ ) -> assert false
  | (KillSetCommand, args) ->
    Format.fprintf ff "<span class=\"keyword\">kill</span>(%a)" (QUICr.Listx.fmt_str_sep ", " Format.pp_print_string) args
  | (KillNumCommand, args) ->
    Format.fprintf ff "<span class=\"keyword\">kill</span>(%a)" (QUICr.Listx.fmt_str_sep ", " Format.pp_print_string) args
  | (SingletonSetCommand, [result;base]) ->
    Format.fprintf ff "%s = {%s}" result base
  | (SingletonSetCommand, _ ) -> assert false
  | (EmptySetCommand, [result]) ->
    Format.fprintf ff "%s = {}" result
  | (EmptySetCommand, _ ) -> assert false
  | (UnionSetCommand, result::args) ->
    Format.fprintf ff "%s = %a" result (QUICr.Listx.fmt_str_sep " | " Format.pp_print_string) args
  | (UnionSetCommand, _) -> assert false
  | (InterSetCommand, result::args) ->
    Format.fprintf ff "%s = %a" result (QUICr.Listx.fmt_str_sep " &amp; " Format.pp_print_string) args
  | (InterSetCommand, _) -> assert false
  | (ChooseCommand, [result; set]) ->
    Format.fprintf ff "%s = <span class=\"keyword\">choose</span>(%s)" result set
  | (ChooseCommand, _) -> assert false
  end;
  Format.fprintf ff ";<br/>@,"
  
and fmt_guard is_assertion indent ff guard =
  (if is_assertion then
    Format.fprintf ff "%a<span class=\"keyword\">verify</span>(%a);<br/>@," fmt_indent indent
  else
    Format.fprintf ff "%a<span class=\"keyword\">assume</span>(%a);<br/>@," fmt_indent indent)
    (fun ff (guard_name, pos, arguments) ->
      let arguments = List.map fmt_arg arguments in
      match guard_name, arguments with
      | (SubSetEqGuard, [op1; op2]) -> Format.fprintf ff "%s &lt; %s" op1 op2
      | (SubSetEqGuard, _) -> assert false
      | (SetEqGuard, [op1; op2]) -> Format.fprintf ff "%s == %s" op1 op2
      | (SetEqGuard, _) -> assert false
      | (LessThanGuard, [op1; op2]) -> Format.fprintf ff "%s &lt; %s" op1 op2
      | (LessThanGuard, _) -> assert false
      | (LessEqGuard, [op1; op2]) -> Format.fprintf ff "%s &lt;= %s" op1 op2
      | (LessEqGuard, _) -> assert false
      | (EqualGuard, [op1; op2]) -> Format.fprintf ff "%s == %s" op1 op2
      | (EqualGuard, _) -> assert false
      | (NotEqualGuard, [op1; op2]) -> Format.fprintf ff "%s != %s" op1 op2
      | (NotEqualGuard, _) -> assert false
    )
    guard

    

let fmt_decls ff variables =
  SMap.iter (fun var typ ->
    Format.fprintf ff
      "<span class=\"keyword\">var</span>&nbsp;%s:&nbsp;<span class=\"type\">%a</span>;<br/>@,"
      (fmt_arg var) QUICr.Types.fmt typ
  ) variables

let print_stylesheet ff =
  Format.fprintf ff "
.keyword {@,
    font-weight: bold;@,
}@,
@,
.type {@,
    font-style: italic;@,
    color: green;@,
}@,
@,
.parameter {@,
    font-style: italic;@,
}@,
@,
.code {@,
    position: relative;@,
    left: 0px;@,
    margin-left: 50px;@,
    margin-right: 50px;@,
    font-family: monospace;@,
    font-size: 150%%;@,
    background-color: lightyellow;@,
}@,
@,
.annotation {@,
    //background-color: gray;@,
    font-family: serif;@,
    color: darkred;@,
}@,
@,
.MathJax_Display .MathJax, @,
DIV.MathJax_MathML math, @,
.MathJax_SVG_Display .MathJax_SVG { @,
    display: inline-block ! important; @,
    background-color: red; @,
    padding: 10px ! important; @,
    border: 2px dashed blue ! important; @,
} @,
@,
.fname {@,
    font-size: 300%%;@,
    text-align: center;@,
    color: darkblue;@,
    font-family: sans-serif;@,
}@,
@,
.number {@,
    color: blue;@,
}@,
@,
.string {@,
    color: red;@,
}@,
"

let print_preamble ff name =
  Format.fprintf ff "<html>@,";
  Format.fprintf ff "<head>@,";
  Format.fprintf ff "<script type=\"text/x-mathjax-config\">@,";
  Format.fprintf ff "  MathJax.Hub.Config({@,";
  Format.fprintf ff "    \"HTML-CSS\": { linebreaks: { automatic: true } },@,";
  Format.fprintf ff "    SVG: { linebreaks: { automatic: true } }@,";
  Format.fprintf ff "  });@,";
  Format.fprintf ff "</script>@,";
  Format.fprintf ff "<script type=\"text/javascript\"@,";
  Format.fprintf ff "        src=\"http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML\">@,";
  Format.fprintf ff "</script>@,";
  (*Format.fprintf ff "<link rel=\"StyleSheet\" href=\"annotated.css\" type=\"text/css\"/>";*)
  Format.fprintf ff "<style type=\"text/css\">@,";
  print_stylesheet ff;
  Format.fprintf ff "</style>@,";
  Format.fprintf ff "</head>@,";
  Format.fprintf ff "<body>@,";
  begin match name with
  | None -> ()
  | Some name ->
    Format.fprintf ff "<div class=\"fname\">%s</div>@," name
  end;
  Format.fprintf ff "<div class=\"code\">@,"

let print_epilog ff =
  Format.fprintf ff "</div>@,";
  Format.fprintf ff "</body>@,";
  Format.fprintf ff "</html>@,"

let fmt ?name:(name=None) fmt_post_state ff pgm =
  Format.fprintf ff "@[<v 0>";
  print_preamble ff name;
  fmt_decls ff pgm.variables;
  Format.fprintf ff "<br/>@,%a@," (fmt_annotation indent_amt fmt_post_state) pgm.init_id;
  fmt_control 0 fmt_post_state ff pgm.program;
  print_epilog ff;
  Format.fprintf ff "@]"


open QUICr.Commands
  
let rec fmt_control fmt_post_state ff = function
  | BlockControl (block, id) ->
    fmt_block ff block;
    Format.fprintf ff "@[<v 2>  %a@]" fmt_post_state id
  | SeqControl (seq) ->
    fmt_seq fmt_post_state ff seq
  | IfControl(thenc, elsec, id) ->
    fmt_if fmt_post_state ff thenc elsec;
    Format.fprintf ff "@[<v 2>  %a@]" fmt_post_state id
  | WhileControl (inv_id, body, post_id) ->
    fmt_while fmt_post_state inv_id ff body;
    Format.fprintf ff "@[<v 2>  %a@]" fmt_post_state post_id

and fmt_while fmt_post_state inv_id ff body =
  Format.fprintf ff "@[<v 0>while(*)@,@[<v 2>  %a@]@,@[<v 2>{@,%a@]@,}@]" fmt_post_state inv_id (fmt_control fmt_post_state) body;

and fmt_if fmt_post_state ff thenc elsec =
  Format.fprintf ff "@[<v 0>@[<v 2>if(*) {@,%a@]@,@[<v 2>} else {@,%a@]@,}@]" (fmt_control fmt_post_state) thenc (fmt_control fmt_post_state) elsec

and fmt_seq fmt_post_state ff seq =
  Format.fprintf ff "@[<v 0>";
  QUICr.Listx.fmt (fun ff -> Format.fprintf ff "@,") (fmt_control fmt_post_state) ff seq;
  Format.fprintf ff "@]"

and fmt_block ff block =
  Format.fprintf ff "@[<v 0>[@,";
  QUICr.Listx.fmt (fun ff -> Format.fprintf ff "@,") (fmt_guard true) ff block.assertions;
  Format.fprintf ff "@,";
  GSet.fmt ~sep:(fun ff -> Format.fprintf ff "@,") (fmt_guard false) ff block.guards;
  Format.fprintf ff "@,";
  QUICr.Listx.fmt (fun ff -> Format.fprintf ff "@,") fmt_command ff block.commands;
  Format.fprintf ff "]@,@]"

and fmt_command ff (name, position, args) =
  match name, args with
  | (SubSetCommand, [result; set; index]) ->
    Format.fprintf ff "%s = %s - %s" result set index
  | (SubSetCommand, _ ) -> assert false
  | (AddCommand, result::args) ->
    Format.fprintf ff "%s = %a" result (QUICr.Listx.fmt_str_sep " + " (fun ff s -> Format.fprintf ff "%s" s)) args
  | (AddCommand, _ ) -> assert false
  | (NegCommand, [result; operand]) ->
    Format.fprintf ff "%s = -%s" result operand
  | (NegCommand, _ ) -> assert false
  | (AssignSetCommand, [result; operand]) ->
    Format.fprintf ff "%s = %s" result operand
  | (AssignNumCommand, [result; operand]) ->
    Format.fprintf ff "%s = %s" result operand
  | (AssignSetCommand, _ ) -> assert false
  | (AssignNumCommand, _ ) -> assert false
  | (ConstCommand const, [result]) ->
    Format.fprintf ff "%s = %a" result AST.fmt_const const
  | (ConstCommand _, _ ) -> assert false
  | (KillSetCommand, args) ->
    Format.fprintf ff "kill(%a)" (QUICr.Listx.fmt_str_sep ", " Format.pp_print_string) args
  | (KillNumCommand, args) ->
    Format.fprintf ff "kill(%a)" (QUICr.Listx.fmt_str_sep ", " Format.pp_print_string) args
  | (SingletonSetCommand, [result;base]) ->
    Format.fprintf ff "%s = {%s}" result base
  | (SingletonSetCommand, _ ) -> assert false
  | (EmptySetCommand, [result]) ->
    Format.fprintf ff "%s = {}" result
  | (EmptySetCommand, _ ) -> assert false
  | (UnionSetCommand, result::args) ->
    Format.fprintf ff "%s = %a" result (QUICr.Listx.fmt_str_sep " | " Format.pp_print_string) args
  | (UnionSetCommand, _) -> assert false
  | (InterSetCommand, result::args) ->
    Format.fprintf ff "%s = %a" result (QUICr.Listx.fmt_str_sep " & " Format.pp_print_string) args
  | (InterSetCommand, _) -> assert false
  | (ChooseCommand, [result; set]) ->
    Format.fprintf ff "%s = choose(%s)" result set
  | (ChooseCommand, _) -> assert false

  

and fmt_guard is_assertion ff guard =
  (if is_assertion then
    Format.fprintf ff "verify(%a)"
  else
    Format.fprintf ff "assume(%a)")
    (fun ff (guard_name, pos, arguments) -> match guard_name, arguments with
    | (SubSetEqGuard, [op1; op2]) -> Format.fprintf ff "%s < %s" op1 op2
    | (SubSetEqGuard, _) -> assert false
    | (SetEqGuard, [op1; op2]) -> Format.fprintf ff "%s == %s" op1 op2
    | (SetEqGuard, _) -> assert false
    | (LessThanGuard, [op1; op2]) -> Format.fprintf ff "%s < %s" op1 op2
    | (LessThanGuard, _) -> assert false
    | (LessEqGuard, [op1; op2]) -> Format.fprintf ff "%s <= %s" op1 op2
    | (LessEqGuard, _) -> assert false
    | (EqualGuard, [op1; op2]) -> Format.fprintf ff "%s == %s" op1 op2
    | (EqualGuard, _) -> assert false
    | (NotEqualGuard, [op1; op2]) -> Format.fprintf ff "%s != %s" op1 op2
    | (NotEqualGuard, _) -> assert false
    )
    guard
  
    

  
    


let fmt fmt_post_state ff pgm =
  Format.fprintf ff "@[<v 0>";
  AST.fmt_decls ff pgm.variables;
  Format.fprintf ff "@,@[<v 2>  %a@]@," fmt_post_state pgm.init_id;
  fmt_control fmt_post_state ff pgm.program;
  Format.fprintf ff "@]"

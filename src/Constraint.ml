type uop =
  | Neg
      
type bop =
  | Add
  | Sub
  | Mul
  | Div

type const =
  | ConstNum of float
  | ConstString of string
      
type expr =
  | Const of const
  | Nondet of Types.TSet.t
  | Var of string
  | Uop of uop * expr
  | Bop of bop * expr * expr

type cmp_type =
  | Eq
  | Lt
  | Le
  | Ne

type cnstr =
  | Numeric of expr * cmp_type * expr
  | Has of string * expr
  | Not of cnstr

type update =
  | Assign of string * expr
  | Read of string * string * expr
  | Write of string * string * expr * expr
  | Delete of string * string * expr
  | Empty of string



let mk_nondet tset =
  Nondet tset

let mk_number n =
  Const (ConstNum n)

let mk_string s =
  Const (ConstString s)

let mk_variable v =
  Var v

let mk_neg = function
  | Uop(Neg,e) -> e
  | _ as e -> Uop(Neg,e)

let mk_add a b =
  Bop (Add, a, b)

let mk_sub a b =
  Bop (Sub, a, b)

let mk_mul a b =
  Bop (Mul, a, b)

let mk_div a b =
  Bop (Div, a, b)

let mk_eq a b =
  Numeric (a, Eq, b)

let mk_ne a b =
  Numeric (a, Ne, b)

let mk_lt a b =
  Numeric (a, Lt, b)

let mk_le a b =
  Numeric (a, Le, b)

let mk_gt a b =
  Numeric (b, Lt, a)

let mk_ge a b =
  Numeric (b, Le, a)

let mk_has d e =
  Has(d,e)

let mk_not = function
  | Not c -> c
  | _ as c -> Not c
    
  
let mk_assign v e =
  Assign(v,e)

let mk_read v d e =
  Read(v, d, e)

let mk_write v d i e =
  Write(v, d, i, e)

let mk_delete v d i =
  Delete(v, d, i)

let mk_empty d =
  Empty(d)

let fmt_uop ff = function
  | Neg -> Format.fprintf ff "-"

let fmt_bop ff = function
  | Add -> Format.fprintf ff "+"
  | Sub -> Format.fprintf ff "-"
  | Mul -> Format.fprintf ff "*"
  | Div -> Format.fprintf ff "/"

let fmt_const ff = function
  | ConstNum f ->
    Format.fprintf ff "%.0f" f
  | ConstString s ->
    Format.fprintf ff "\"%s\"" s

let rec fmt_expr ff = function
  | Const c -> fmt_const ff c
  | Nondet tset -> Format.fprintf ff "nondet(%a)" AST.fmt_types tset
  | Var v -> Format.fprintf ff "%s" v
  | Uop(op,e) -> Format.fprintf ff "(%a%a)" fmt_uop op fmt_expr e
  | Bop(op,a,b) -> Format.fprintf ff "(%a %a %a)" fmt_expr a fmt_bop op fmt_expr b

let fmt_ct ff = function
  | Eq ->
    Format.fprintf ff "=="
  | Ne ->
    Format.fprintf ff "!="
  | Lt ->
    Format.fprintf ff "<"
  | Le ->
    Format.fprintf ff "<="


let rec fmt_cnstr ff = function
  | Numeric(a, op, b) ->
    Format.fprintf ff "%a %a %a" fmt_expr a fmt_ct op fmt_expr b
  | Has(d, e) ->
    Format.fprintf ff "%s.has[%a]" d fmt_expr e
  | Not c ->
    Format.fprintf ff "!%a" fmt_cnstr c

let fmt_update ff = function
  | Assign(v, e) ->
    Format.fprintf ff "%s = %a" v fmt_expr e
  | Read(v, d, i) ->
    Format.fprintf ff "%s = %s[%a]" v d fmt_expr i
  | Write(v, d, i, e) ->
    Format.fprintf ff "%s = (%s[%a] = %a)" v d fmt_expr i fmt_expr e
  | Delete(v, d, i) ->
    Format.fprintf ff "%s = %s.del[%a]" v d fmt_expr i
  | Empty(d) ->
    Format.fprintf ff "%s = {}" d

module SMap = Map.Make(struct
  type t = string
  let compare = compare
end)

let op_merge op a b =
  SMap.merge (fun k v1 v2 ->
    match (v1, v2) with
      | (None, None) -> None
      | (Some o, None)
      | (None, Some o) -> Some o
      | (Some o1, Some o2) -> Some (op o1 o2)
  ) a b

    (*

let rec linearize_expr = function
  | Const f -> Some(SMap.empty, f)
  | Nondet -> None
  | Var v -> Some(SMap.singleton v 1.0, 0.0)
  | Uop(op, e) ->
    linearize_uop op e
  | Bop(op,a,b) ->
    linearize_bop op a b
and linearize_uop op e =
  let e = linearize_expr e in
  match (e,op) with
    | (None,_) -> None
    | (Some (coeff, const), Neg) -> Some(SMap.map (fun c -> (-. c)) coeff, (-. const))
and linearize_bop op a b =
  let a = linearize_expr a in
  let b = linearize_expr b in
  match (op, a, b) with
    | (_, None, _)
    | (_, _, None) -> None
    | (Add, Some(coeff1, const1), Some(coeff2, const2)) ->
      Some(op_merge (fun c1 c2 -> c1 +. c2) coeff1 coeff2, const1 +. const2)
    | (Sub, Some(coeff1, const1), Some(coeff2, const2)) ->
      Some(op_merge (fun c1 c2 -> c1 -. c2) coeff1 coeff2, const1 -. const2)
    | (Mul, Some(coeff1, const1), Some(coeff2, const2)) ->
      if SMap.is_empty coeff1 && SMap.is_empty coeff2 then
        Some(SMap.empty, const1 *. const2)
      else
        None
    | (Div, Some(coeff1, const1), Some(coeff2, const2)) ->
      if SMap.is_empty coeff1 && SMap.is_empty coeff2 then
        Some(SMap.empty, const1 /. const2)
      else
        None

let texpr_of_linexpr = function
  | None -> Nondet
  | Some(coeffs, const) ->
    let lcoeffs = SMap.fold (fun var coeff rest ->
      let el = match coeff with
        | (1.0) -> Var var
        | (-1.0) -> Uop(Neg, Var var)
        | (0.0)
        | (-0.0) -> Const 0.0
        | o -> Bop(Mul, Const o, Var var)
      in
      el::rest
    ) coeffs [] in
    let rec sum_list = function
      | [] -> Const const
      | [h] ->
        if (classify_float const) = FP_zero then
          h
        else
          Bop(Add, h, Const const)
      | h::t -> Bop(Add, h, sum_list t)
    in
    sum_list lcoeffs
    

let linearize_expr e =
  let linexp = linearize_expr e in
  texpr_of_linexpr linexp

let rec linearize_cnstr = function
  | Numeric(e1,op,e2) ->
    let e = mk_sub e1 e2 in
    Numeric(linearize_expr e, op, Const 0.0)
  | Has(d, e) ->
    Has(d, linearize_expr e)
  | Not(c) ->
    Not(linearize_cnstr c)

let linearize_update = function
  | Assign(v, e) ->
    Assign(v, linearize_expr e)
  | Read(v, d, i) ->
    Read(v, d, linearize_expr i)
  | Write(d, i, e) ->
    Write(d, linearize_expr i, linearize_expr e)
  | Delete(d, i) ->
    Delete(d, linearize_expr i)
  | Empty(d) ->
    Empty(d)

    

    *) 

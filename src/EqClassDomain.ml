module EqClassDomain : QUICr.Domain.ScalarDomain = struct
  module Commands = QUICr.Commands
  type ctx = unit
  type elem =
    | Const of int
    | Dim of int

  module Eq = UnionFind.Make(struct
    type t = elem
    let compare = compare
  end)

  type t = {
    dim: int;
    st: Eq.t;
  }

  let args = []

  let init () = ()

  let top ctx dim =
    {
      dim = dim;
      st = QUICr.Listx.fold_seq (fun i st -> Eq.add st (Dim i)) 0 dim Eq.empty;
    }

  let bottom ctx dim =
    let st = QUICr.Listx.fold_seq (fun i st -> Eq.add st (Dim i)) 0 dim Eq.empty in
    let st = Eq.union st (Const 0) (Const 1) in
    {
      dim = dim;
      st;
    }

  let add_dim ctx st =
    { 
      dim = st.dim + 1;
      st = Eq.add st.st (Dim (st.dim));
    }

  let rem_dim ctx st =
    {
      dim = st.dim - 1;
      st = Eq.remove st.st (Dim (st.dim - 1));
    }

  let get_dim ctx st =
    st.dim

  let get_eq ctx st =
    let eqs = Eq.equivalence_classes st.st in
    List.rev_map
      (fun s ->
        QUICr.Listx.map_filter (function
          | Dim i -> Some i
          | _ -> None
        ) s
      )
      eqs



  let is_top ctx st =
    List.for_all (fun l -> (List.length l) = 1) (Eq.equivalence_classes st.st)

  let is_bottom ctx st =
    List.exists (fun l ->
      (List.length (List.filter (function
        | Const _ -> true
        | Dim _ -> false
      ) l)) > 1
    ) (Eq.equivalence_classes st.st)

  let narrow ctx a b =
    b



  let meet ctx a b =
    assert (a.dim = b.dim);
    let eqs = Eq.equivalence_classes b.st in
    { a with
      st = List.fold_left (fun a eq ->
        Eq.union_list a eq
      ) a.st eqs
    }

  let fmt_elem fmt_symb ff = function
    | Const i -> Format.fprintf ff "%d" i
    | Dim i -> fmt_symb ff i

  let fmt ?escape:(escape=FormatHelp.Text) ctx fmt_symb ff st =
    let eqs = Eq.equivalence_classes st.st in
    let eqs = List.filter (fun l -> (List.length l) > 1) eqs in
    QUICr.Listx.fmt
      (fun ff -> FormatHelp.fmt_conj escape ff ())
      (fun ff eq ->
        QUICr.Listx.fmt
          (fun ff -> FormatHelp.fmt_eq escape ff ())
          (fun ff -> function
            | Const i -> Format.fprintf ff "%d" i
            | Dim i -> fmt_symb ff i
          )
          ff
          eq
      )
      ff
      eqs

  let fmt_elem_default ff el =
    fmt_elem (fun ff v -> Format.fprintf ff "v%d" v) ff el

  let fmt_default ctx ff st =
    fmt ctx (fun ff v -> Format.fprintf ff "v%d" v) ff st

  let le ctx a b =
    (*Format.printf "LE:@.";
    Format.printf " a: %a@." (fmt_default ctx) a;
    Format.printf " b: %a@." (fmt_default ctx) b;*)
    let (ast,bst) = Eq.unify a.st b.st in
    let a = {a with st = ast} in
    let b = {b with st = bst} in
    (* true if every equivalence class in b is completely contained in an equivalence class in a *)
    if is_bottom ctx a then
      true
    else if is_bottom ctx b then
      false
    else
      let eqsa = Eq.equivalence_classes a.st in
      let eqsb = Eq.equivalence_classes b.st in
      List.for_all (fun eqb ->
        List.exists (fun eqa ->
          List.for_all (fun elb ->
            List.exists (fun ela ->
              (*Format.printf "Check %a = %a@." fmt_elem_default ela fmt_elem_default elb;*)
              ela = elb
            ) eqa
          ) eqb
        ) eqsa
      ) eqsb

  let transition ctx st (cmd, pos, args) =
    match cmd with
      | Commands.AddCommand ->
          begin match args with
            | [res;arg1;arg2] ->
                let cnst1 = List.fold_left (fun res el ->
                  match (res,el) with
                    | Some i, _ -> Some i
                    | None, Const i -> Some i
                    | None, Dim _ -> None
                ) None (Eq.equivalent st.st (Dim arg1)) in
                let cnst2 = List.fold_left (fun res el ->
                  match (res,el) with
                    | Some i, _ -> Some i
                    | None, Const i -> Some i
                    | None, Dim _ -> None
                ) None (Eq.equivalent st.st (Dim arg2)) in
                begin match cnst1,cnst2 with
                  | None,_
                  | _, None ->
                      let stst = Eq.remove st.st (Dim res) in
                      { st with st = Eq.add stst (Dim res) }
                  | Some c1, Some c2 ->
                      let stst = Eq.remove st.st (Dim res) in
                      { st with st = Eq.union stst (Dim res) (Const (c1 + c2)) }
                end
            | _ ->
                assert false
          end
      | Commands.NegCommand ->
          begin match args with
            | [res;arg] ->
                let cnst = List.fold_left (fun res el ->
                  match (res,el) with
                    | Some i, _ -> Some i
                    | None, Const i -> Some i
                    | None, Dim _ -> None
                ) None (Eq.equivalent st.st (Dim arg)) in
                begin match cnst with
                  | None ->
                      let stst = Eq.remove st.st (Dim res) in
                      { st with st = Eq.add stst (Dim res) }
                  | Some i ->
                      let stst = Eq.remove st.st (Dim res) in
                      { st with st = Eq.union stst (Dim res) (Const (-i)) }
                end
            | _ ->
                assert false
          end
      | Commands.AssignNumCommand ->
          begin match args with
            | [res;arg] ->
                let stst = Eq.remove st.st (Dim res) in
                { st with st = Eq.union stst (Dim res) (Dim arg) }
            | _ ->
                assert false
          end
      | Commands.ConstCommand (QUICr.Const.NumConst c) ->
          begin match args with
            | [res] ->
                let stst = Eq.remove st.st (Dim res) in
                { st with st = Eq.union stst (Dim res) (Const c) }
            | _ ->
                assert false
          end
      | Commands.KillNumCommand ->
          List.fold_left (fun st arg ->
            let stst = Eq.remove st.st (Dim arg) in
            { st with st = Eq.add stst (Dim arg) }
          ) st args
      | _ ->
          assert false

  let constrain ctx st (guard, pos, args) =
    let (a, b) = match args with
      | [a; b] -> (a,b)
      | _ -> assert false
    in
    match guard with
    | Commands.LessThanGuard
    | Commands.NotEqualGuard ->
        (* not equal -> should not be in same eq class *)
        let ra = Eq.get_representative st.st (Dim a) in
        let rb = Eq.get_representative st.st (Dim b) in
        if ra = rb then
          bottom ctx (get_dim ctx st)
        else
          st
    | Commands.LessEqGuard ->
        st
    | Commands.EqualGuard ->
        let stst = Eq.union st.st (Dim a) (Dim b) in
        { st with
          st = stst
        }
    | _ -> assert false

  let join ctx a b =
    assert (a.dim = b.dim);
    {
      dim = a.dim;
      st = Eq.partition ~fmt:(Some (fun ff v ->
        match v with
          | Dim v -> Format.fprintf ff "v%d" v
          | Const i -> Format.fprintf ff "%d" i
      )) a.st b.st;
    }

  let widen ctx a b =
    join ctx a b

end
    

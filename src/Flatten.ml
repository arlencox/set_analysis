open QUICr.Common
open AST
open ASTType

type state_t =
  {
    vars : QUICr.Types.t SMap.t ref;
    pgm : program;
  }


let rec flatten_commands state = function
  | [] -> []
  | h::t ->
    let cmds1 = flatten_command state h in
    let cmds2 = flatten_commands state t in
    cmds1 @ cmds2

and flatten_command state = function
  | IfCommand(pos, cond, thenb, elseb) ->
    let (cmds,cond) = flatten_expr true state cond in
    let thenb = flatten_commands state thenb in
    let elseb = flatten_commands state elseb in
    cmds @ [IfCommand(pos, cond, thenb, elseb)]
  | WhileCommand(pos, cond, body) ->
    let (cmds,cond) = flatten_expr true state cond in
    let body = flatten_commands state body in
    cmds @ [WhileCommand(pos, cond, body @ cmds)]
  | ForEachCommand(pos, var, expr, body) ->
    let (cmds,expr) = flatten_expr true state expr in
    let body = flatten_commands state body in
    cmds @ [ForEachCommand(pos, var, expr, body @ cmds)]
  | AssignCommand(pos, var, e) ->
    let (cmds,e) = flatten_expr true state e in
    let v_type = SMap.find var !(state.vars) in
    let e_t = expr_type !(state.vars) e in
    if v_type <> e_t then
      type_error e ("Expected "^(QUICr.Types.toString v_type)^" type");
    cmds @ [AssignCommand(pos, var, e)]
  | AssertCommand(pos, e) ->
    let (cmds,e) = flatten_expr true state e in
    cmds @ [AssertCommand(pos, e)]
  | AssumeCommand(pos, e) ->
    let (cmds,e) = flatten_expr true state e in
    cmds @ [AssumeCommand(pos, e)]
  | KillCommand(pos, vars) ->
    [KillCommand(pos, vars)]

and flatten_expr is_top_level state ei =
  let (cmds, typ, e) = match ei with
    | BinaryExpr(pos, op, e1, e2) ->
      let (cmds1,e1) = flatten_expr false state e1 in
      let (cmds2,e2) = flatten_expr false state e2 in
      (cmds1 @ cmds2, expr_type !(state.vars) ei, BinaryExpr(pos, op, e1, e2))
    | UnaryExpr(pos, op, e) ->
      let (cmds,e) = flatten_expr false state e in
      (cmds, expr_type !(state.vars) ei, UnaryExpr(pos, op, e))
    | ConstExpr(pos, c) ->
      ([], expr_type !(state.vars) ei, ConstExpr(pos, c))
    | VarExpr(pos, v) ->
      ([], expr_type !(state.vars) ei, VarExpr(pos, v))
    | EmptySetExpr pos ->
      ([], expr_type !(state.vars) ei, EmptySetExpr pos)
    | CallExpr (pos,name,args) ->
      (* TODO: flatten call expr *)
      ([], expr_type !(state.vars) ei, CallExpr (pos,name,args))
  in
  let generate_var = match e with
    | VarExpr _ -> false
    | _ -> not is_top_level
  in
  if generate_var then
    let v = fresh_var state.pgm in
    state.vars := SMap.add v typ !(state.vars);
    (cmds @ [AssignCommand(expr_pos ei,  v, e)], VarExpr(expr_pos ei, v))
  else
    (cmds, e)
    

let flatten p =
  let state =
    {
      vars = ref p.variables;
      pgm = p;
    }
  in
  let commands = flatten_commands state p.program in
  {
    variables = !(state.vars);
    var_tmp = state.pgm.var_tmp;
    program = commands
  }

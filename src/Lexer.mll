{
  open Lexing
  open Parser
  open QUICr.Common

  let incr_lineno lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <- { pos with
    pos_lnum = pos.pos_lnum + 1;
    pos_bol = pos.pos_cnum;
    }

}

let ident = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '_' '0'-'9']*
let digit = ['0'-'9']

rule token = parse
  | [' ' '\t'] { token lexbuf }
  | ['\n'] { incr_lineno lexbuf; token lexbuf }
  | "//" { comment lexbuf}
  | "while" { WHILE }
  | "if" { IF }
  | "else" { ELSE }
  | "for" { FOR }
  | "in"  { IN }
  | "num" { NUM }
  | "set" { SET }
  | "var" { VAR }
  | ":" { COLON }
  | "," { COMMA }
  | ident as name { IDENT name }
  | "|" { UNION }
  | "&" { INTER }
  | "(" { LPAREN }
  | ")" { RPAREN }
  | "{" { LCURLY }
  | "}" { RCURLY }
  | ";" { SEMI }
  | "+" { PLUS }
  | "-" { MINUS }
  | "*" { STAR }
  | "{}" { EMPTYSET }
  | "=" { ASSIGN }
  | "==" { EQUALS }
  | "!=" { NOTEQUALS }
  | "<" { LESSTHAN }
  | "<=" { LESSEQ }
  | ">" { GREATERTHAN }
  | ">=" { GREATEREQ }
  | digit+ as f { INT (int_of_string f) }
  | eof { EOF }
  | _ as c
      { raise (Lexing_error (lexbuf.lex_curr_p, "Unexpected Character: '"^(String.make 1 c)^"'")) }

and comment = parse
  | "\n" { incr_lineno lexbuf; token lexbuf }
  | eof { EOF }
  | _ { comment lexbuf }

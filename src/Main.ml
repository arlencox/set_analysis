open QUICr.Common

type apron_dom_sel =
  | AP_Poly
  | AP_Oct
  (*| AP_Int*)
  | Eq_Class

let apron_dom_sel = ref AP_Poly

let args = [
  ("-poly", Arg.Unit (fun () -> apron_dom_sel := AP_Poly), "Run with Polka polyhedral analysis domain");
  ("-oct", Arg.Unit (fun () -> apron_dom_sel := AP_Oct), "Run with octagon analysis domain");
  (*("-int", Arg.Unit (fun () -> apron_dom_sel := AP_Int), "Run with interval analysis domain");*)
  ("-eq", Arg.Unit (fun () -> apron_dom_sel := Eq_Class), "Run with equivalence class domain");
  ("-verbose", Arg.Set Logger.verbose, "Print out (lots) of information")
]


  
let write_to_file fname fmt structure =
  let ch = open_out fname in
  let ff = Format.formatter_of_out_channel ch in
  Format.fprintf ff "%a@." fmt structure;
  close_out ch

let print_location l =
  Printf.printf "%d:%d" l.Lexing.pos_lnum (l.Lexing.pos_cnum - l.Lexing.pos_bol + 1)


let parse_file channel base_name =
  (* parse the input files *)
  let lexbuf = Lexing.from_channel channel in
  Parser.program Lexer.token lexbuf

let run_analysis channel base_name =
      let module ND = (val (match !apron_dom_sel with
        | AP_Poly ->
            (module ApronDomain.PolkaDomain : QUICr.Domain.ScalarDomain)
        | AP_Oct ->
            (module ApronDomain.OctDomain : QUICr.Domain.ScalarDomain)
        (*| AP_Int ->
            (module ApronDomain.BoxDomain : QUICr.Domain.ScalarDomain)*)
        | Eq_Class ->
            (module EqClassDomain.EqClassDomain : QUICr.Domain.ScalarDomain)) : QUICr.Domain.ScalarDomain)
      in
      let module SD = QUICr.BottomDomain.Make(QUICr.SetDomain.Make(ND)) in
      let module ED = QUICr.EnvDomain.Make(SD) in
      let module SE = SymbolicExecutor.Make(ED) in
      try
        (* parse the input files *)
        let ast = parse_file channel base_name in
        write_to_file (base_name^".ast") AST.fmt ast;
        let ast = Flatten.flatten ast in
        write_to_file (base_name^".flat.ast") AST.fmt ast;
        let cmd = AST2Command.convert ast in
        let ctx = ED.init () in
        let annotations = SE.execute ctx cmd in
        let fmt_post_state escape ff id =
          let st = IMap.find id annotations in
          ED.fmt ~escape:escape ctx ff st
        in
        write_to_file (base_name^".cmd") (CommandsText.fmt (fmt_post_state FormatHelp.Text)) cmd;
        write_to_file (base_name^".html") (CommandsHTML.fmt ~name:(Some (base_name^".js0")) (fmt_post_state FormatHelp.HTML)) cmd
      with
        | QUICr.Common.Parse_error(start,stop,msg) ->
            Printf.printf "Error: ";
            print_location start;
            Printf.printf "-";
            print_location stop;
            Printf.printf "  -  %s\n" msg;
            exit (-1)
              
        | QUICr.Common.Lexing_error(pos, msg) ->
            Printf.printf "Error: ";
            print_location pos;
            Printf.printf "  -  %s\n" msg;
            exit (-1)


let run_file fname =
  let ch = open_in (fname^".js0") in
  run_analysis ch fname;
  close_in ch

let run_stdin () =
  let ch = stdin in
  run_analysis ch "stdin"


let process_arguments () =
  let files_to_process = ref [] in
  let add_file f =
    (* check file extension *)
    let extension = Str.regexp "\\(.*\\)\\.js0$" in
    if Str.string_match extension f 0 then begin
      let base_name = Str.matched_group 1 f in
      files_to_process := base_name :: !files_to_process
    end else
      raise (Arg.Bad "Files must end in the .js0 extension")
  in
  let usage = "Runs an abstract interpreter on a simple language involving sets and numbers" in
  Arg.parse (
    (*ED.args @*)
    QUICr.Timeout.args @
      args @
      QUICr.SetDomain.args @
      SymbolicExecutor.args @
      QUICr.SetConstraintGraph.args @
      ApronDomain.args @
      QUICr.EnvDomain.args
  ) add_file usage;
  let files_to_process = List.rev !files_to_process in
  if files_to_process = [] then
    run_stdin ()
  else
    List.iter run_file files_to_process
    
  
let _ =
  process_arguments ()

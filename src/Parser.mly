%{
  open Parsing
  open AST
  open QUICr.Common
  open QUICr.Types

  type pi_types =
    | PIVar
    | PICall of expr list
%}

%token <int> INT
%token <string> IDENT
%token COLON
%token VAR
%token ASSERT
%token ASSUME
%token IF
%token FOR
%token IN
%token ELSE
%token WHILE
%token NUM
%token SET
%token LPAREN
%token RPAREN
%token LCURLY
%token RCURLY
%token PLUS
%token MINUS
%token EMPTYSET
%token ASSIGN
%token EQUALS
%token NOTEQUALS
%token LESSTHAN
%token LESSEQ
%token GREATERTHAN
%token GREATEREQ
%token SEMI
%token COMMA
%token UNION
%token INTER
%token STAR
%token EOF


%start program
%type <AST.program> program

%nonassoc LESSTHAN LESSEQ GREATERTHAN GREATEREQ NOTEQUALS EQUALS
%left ASSIGN
%left UNION
%left INTER
%left PLUS MINUS
%nonassoc UMINUS

%%

program:
  type_decls_opt block EOF {
      {
        variables = $1;
        program = $2;
        var_tmp = ref 0;
      }
    }
 | error
     {
       let start_pos = Parsing.rhs_start_pos 1 in
       let end_pos = Parsing.rhs_end_pos 1 in
       raise (Parse_error (start_pos, end_pos, "Expected variable declarations or statement"))
     }
  ;

type_decls_opt
  :              { SMap.empty }
  | type_decls   { $1 }
  ;

type_decls
  : VAR IDENT COLON typ SEMI            { SMap.singleton $2 $4 }
  | type_decls VAR IDENT COLON typ SEMI { SMap.add $3 $5 $1 }
  | VAR error SEMI                      {
    let start_pos = Parsing.rhs_start_pos 2 in
    let end_pos = Parsing.rhs_end_pos 2 in
    raise (Parse_error (start_pos, end_pos, "Expected variable name"))
  }
  | VAR IDENT error SEMI                {
    let start_pos = Parsing.rhs_start_pos 3 in
    let end_pos = Parsing.rhs_end_pos 3 in
    raise (Parse_error (start_pos, end_pos, "Expected colon"))
  }
  | VAR IDENT COLON error SEMI          {
    let start_pos = Parsing.rhs_start_pos 4 in
    let end_pos = Parsing.rhs_end_pos 4 in
    raise (Parse_error (start_pos, end_pos, "Expected type"))
  }
  ;

typ
  : NUM { Number }
  | SET { Set }
  ;
  

block
  : command block { $1::$2 }
  |               { [] }
  ;

command
  : IF LPAREN expr RPAREN LCURLY block RCURLY if_tail
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 8) in
        IfCommand(pos,$3,$6,$8)
      }

  | IF error
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected parentheses"))
      }

  | IF LPAREN error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }

  | IF LPAREN expr error
      {
        let start_pos = Parsing.rhs_start_pos 4 in
        let end_pos = Parsing.rhs_end_pos 4 in
        raise (Parse_error (start_pos, end_pos, "Expected closed parenthesis"))
      }

  | IF LPAREN expr RPAREN error
      {
        let start_pos = Parsing.rhs_start_pos 5 in
        let end_pos = Parsing.rhs_end_pos 5 in
        raise (Parse_error (start_pos, end_pos, "Expected new block {"))
      }

  | IF LPAREN expr RPAREN LCURLY error
      {
        let start_pos = Parsing.rhs_start_pos 6 in
        let end_pos = Parsing.rhs_end_pos 6 in
        raise (Parse_error (start_pos, end_pos, "Expected statements"))
      }
  | FOR LPAREN IDENT IN expr RPAREN LCURLY block RCURLY
      {
        let start_pos = Parsing.rhs_start_pos 1 in
        let end_pos = Parsing.rhs_end_pos 9 in
        ForEachCommand((start_pos, end_pos), $3, $5, $8)
      }

  | FOR error
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected open parenthesis"))
      }

  | FOR LPAREN error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected variable"))
      }

  | FOR LPAREN IDENT error
      {
        let start_pos = Parsing.rhs_start_pos 4 in
        let end_pos = Parsing.rhs_end_pos 4 in
        raise (Parse_error (start_pos, end_pos, "Expected \"in\""))
      }

  | FOR LPAREN IDENT IN error
      {
        let start_pos = Parsing.rhs_start_pos 5 in
        let end_pos = Parsing.rhs_end_pos 5 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }

  | FOR LPAREN IDENT IN expr error 
      {
        let start_pos = Parsing.rhs_start_pos 6 in
        let end_pos = Parsing.rhs_end_pos 6 in
        raise (Parse_error (start_pos, end_pos, "Expected close parenthesis"))
      }

  | FOR LPAREN IDENT IN expr RPAREN error 
      {
        let start_pos = Parsing.rhs_start_pos 7 in
        let end_pos = Parsing.rhs_end_pos 7 in
        raise (Parse_error (start_pos, end_pos, "Expected open block {"))
      }

  | FOR LPAREN IDENT IN expr RPAREN LCURLY error 
      {
        let start_pos = Parsing.rhs_start_pos 8 in
        let end_pos = Parsing.rhs_end_pos 8 in
        raise (Parse_error (start_pos, end_pos, "Expected statements"))
      }

  | WHILE LPAREN expr RPAREN LCURLY block RCURLY
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 7) in
        WhileCommand(pos, $3, $6)
      }

  | WHILE error
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected open parenthesis"))
      }

  | WHILE LPAREN error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
      
  | WHILE LPAREN expr error
      {
        let start_pos = Parsing.rhs_start_pos 4 in
        let end_pos = Parsing.rhs_end_pos 4 in
        raise (Parse_error (start_pos, end_pos, "Expected close parenthesis"))
      }

  | WHILE LPAREN expr RPAREN error
      {
        let start_pos = Parsing.rhs_start_pos 5 in
        let end_pos = Parsing.rhs_end_pos 5 in
        raise (Parse_error (start_pos, end_pos, "Expected open block {"))
      }

  | WHILE LPAREN expr RPAREN LCURLY error
      {
        let start_pos = Parsing.rhs_start_pos 6 in
        let end_pos = Parsing.rhs_end_pos 6 in
        raise (Parse_error (start_pos, end_pos, "Expected statements"))
      }

  | IDENT ASSIGN expr SEMI
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 4) in
        AssignCommand(pos, $1,$3)
      }

  | IDENT error SEMI
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected ="))
      }

  | IDENT ASSIGN error SEMI
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr SEMI
      {
        let start_pos = Parsing.rhs_start_pos 1 in
        let end_pos = Parsing.rhs_end_pos 1 in
        match $1 with
        | CallExpr(pos, name, args) ->
          begin match (name, args) with
          | ("kill", args) ->
            let vars = List.map (function
              | VarExpr(_, v) -> v
              | _ ->
                let (spos,epos) = pos in
                raise (Parse_error (spos, epos, "Kill only supports variable arguments"))
            ) args in
            KillCommand(pos, vars)
          | ("assume", [e]) ->
            AssumeCommand(pos, e)
          | ("assert", [e]) ->
            AssertCommand(pos, e)
          | _ ->
            raise (Parse_error (start_pos, end_pos, "Unsupported top level function"))
          end
        | _ ->
          raise (Parse_error (start_pos, end_pos, "Unsupported top level expression"))
      }
  ;

if_tail
  : ELSE LCURLY block RCURLY { $3 }
  |                          { [] }
  | ELSE error
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected new block {"))
      }
  | ELSE LCURLY error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected statements"))
      }
  ;

expr
  : expr PLUS expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BAdd, $1, $3)
      }
  | expr PLUS error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr MINUS expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BSubSet, $1, $3)
      }
  | expr MINUS error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr UNION expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BUnion, $1, $3)
      }
  | expr UNION error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr INTER expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BInter, $1, $3)
      }
  | expr INTER error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | MINUS expr %prec UMINUS
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 2) in
        UnaryExpr(pos, UNeg, $2)
      }
  | MINUS error %prec UMINUS
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr LESSTHAN expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BLt, $1, $3)
      }
  | expr LESSTHAN error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr LESSEQ expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BLe, $1, $3)
      }
  | expr LESSEQ error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr GREATERTHAN expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BLt, $3, $1)
      }
  | expr GREATERTHAN error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr GREATEREQ expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BLe, $3, $1)
      }
  | expr GREATEREQ error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr EQUALS expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BEq, $1, $3)
      }
  | expr EQUALS error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | expr NOTEQUALS expr
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 3) in
        BinaryExpr(pos, BNe, $1, $3)
      }
  | expr NOTEQUALS error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | INT
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 1) in
        ConstExpr(pos, QUICr.Const.NumConst $1)
      }
  | IDENT post_ident
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 2) in
        match $2 with
        | PIVar -> VarExpr (pos, $1)
        | PICall el ->
          begin match ($1,el) with
          | ("choose", [set]) -> UnaryExpr(pos, UChoose, set)
          | ("choose", _) ->
            let (spos,epos) = pos in
            raise (Parse_error (spos,epos, "Incorrect number of arguments to built-in function"))
          | _ -> CallExpr(pos, $1, el)
          end
      }
  | LPAREN expr RPAREN          { $2 }
  | LPAREN error RPAREN
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  | EMPTYSET
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 1) in
        EmptySetExpr(pos)
      }
  | LCURLY opt_arguments RCURLY
      {
        let pos = (Parsing.rhs_start_pos 1, Parsing.rhs_end_pos 1) in
        match $2 with
          | [] -> EmptySetExpr(pos)
          | h::t ->
              List.fold_left (fun union el ->
                BinaryExpr(pos,BUnion,union,UnaryExpr(pos, USingleton, el))
              ) (UnaryExpr(pos, USingleton, h)) t
      }
  | LCURLY error
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected list of set elements"))
      }
  ;


post_ident
  :                             { PIVar }
  | LPAREN opt_arguments RPAREN    { PICall $2 }
  | LPAREN error
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected argument list"))
      }
  ;

opt_arguments
  : arguments  { List.rev $1 }
  |            { [] }
  ;

arguments
  : arguments COMMA expr { $3 :: $1 }
  | expr                 { [$1] }
  | arguments error
      {
        let start_pos = Parsing.rhs_start_pos 2 in
        let end_pos = Parsing.rhs_end_pos 2 in
        raise (Parse_error (start_pos, end_pos, "Expected comma"))
      }
  | arguments COMMA error
      {
        let start_pos = Parsing.rhs_start_pos 3 in
        let end_pos = Parsing.rhs_end_pos 3 in
        raise (Parse_error (start_pos, end_pos, "Expected expression"))
      }
  ;

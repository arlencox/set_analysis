let widening_delay_count      = ref 5
let narrowing_iteration_count = ref 10

let show_pass = ref false

let args = [
  ("-widen-delay", Arg.Set_int widening_delay_count, "Maximum number of iterations before applying widening (default 5)");
  ("-narrowing", Arg.Set_int narrowing_iteration_count, "Maximum number of iterations of narrowing (default 10)");
  ("-show-pass", Arg.Set show_pass, "Show proved assertions")
]

  
module Make(D: QUICr.Domain.EnvDomain) = struct
  open QUICr.Common
  open QUICr.Commands

  module L = Logger.Make(struct let tag="SymbolicExecutor" end)


  type state = {
    env: D.env;
    annotations: D.t IMap.t;
    vars: QUICr.Types.t SMap.t;
  }

  let execute_block ctx env condition block =
    QUICr.Timeout.check_timeout ();
    let condition = List.fold_left (fun condition ((_, pos, _) as guard) ->
      let just_guard = D.top ctx env in
      let just_guard = D.constrain ctx just_guard guard in
      if D.le ctx condition just_guard then begin
        (* passed *)
        if !show_pass then L.msg "%a: %a: pass" (CommandsText.fmt_guard true) guard fmt_position pos
      end else begin
        (* failed *)
        L.msg "%a: %a: FAIL" (CommandsText.fmt_guard true) guard fmt_position pos
      end;
      D.constrain ctx condition guard
    ) condition block.assertions in
    let condition = GSet.fold (fun guard condition ->
      L.dbg "%a@," (CommandsText.fmt_guard false) guard;
      D.constrain ctx condition guard
    ) block.guards condition in
    let condition = List.fold_left (fun condition command ->
      L.dbg "%a@," CommandsText.fmt_command command;
      D.transition ctx condition command) condition block.commands in
    condition

  let rec execute_control ctx state prev_id ctl =
    match ctl with
      | SeqControl [] ->
          (prev_id, state)
      | SeqControl (h::t) ->
          let (prev_id, state) = execute_control ctx state prev_id h in
          execute_control ctx state prev_id (SeqControl t)

      | BlockControl(block, id) ->
          let pre_condition = IMap.find prev_id state.annotations in
          (*L.dbg "%a" CommandsText.fmt_block block;*)
          let result = execute_block ctx state.env pre_condition block in
          L.dbg "  %a@," (D.fmt ctx) result;
          let state =
            { state with
              annotations = IMap.add id result state.annotations } in
          (id, state)

      | IfControl(thenc, elsec, id) ->
          L.dbg "Starting then@,";
          let (then_id, state) = execute_control ctx state prev_id thenc in
          L.dbg "Starting else@,";
          let (else_id, state) = execute_control ctx state prev_id elsec in
          (* get then and else results *)
          let then_result = IMap.find then_id state.annotations in
          let else_result = IMap.find else_id state.annotations in
          L.dbg "Joining results@,";
          (* join the results *)
          let result = D.join ctx then_result else_result in
          L.dbg "  %a@," (D.fmt ctx) result;
          let state =
            { state with
              annotations = IMap.add id result state.annotations } in
          (id, state)

      | WhileControl(inv_id, bodyc, id) ->
          let (state,feedback_id) = widening_iterations ctx state bodyc inv_id prev_id (-1) (!widening_delay_count) in
          assert(feedback_id >= 0);
          let state = narrowing_iterations ctx state bodyc inv_id prev_id feedback_id (!narrowing_iteration_count) in
          let result = IMap.find inv_id state.annotations in
          let state =
            { state with
              annotations = IMap.add id result state.annotations } in
          (id, state)


  and widening_iterations ctx state bodyc inv_id pre_id feedback_id rem_iterations =
    let pre_condition = IMap.find pre_id state.annotations in
    let (state, run_iteration) = if feedback_id < 0 then begin
      L.dbg "Candidate loop invariant: %a@," (D.fmt ctx) pre_condition;
      ({ state with
        annotations = IMap.add inv_id pre_condition state.annotations },
       true)
    end else begin
      let feedback_condition = IMap.find feedback_id state.annotations in
      let invariant_condition = IMap.find inv_id state.annotations in
      assert(D.le ctx pre_condition invariant_condition);
      let complete = D.le ctx feedback_condition invariant_condition in
      if complete then begin
        (* terminating because analysis of loop complete *)
        L.dbg "Loop invariant found.  Continuing.@,";
        (state, false)
      end else begin
        L.dbg "Running join (loop invariant)@,";
        let joined = D.join ctx pre_condition feedback_condition in
        let invariant_condition' = if rem_iterations > 0 then
            joined
          else begin
            (*assert(D.le ctx invariant_condition joined);*)
            L.dbg "Running widen (loop invariant)@,";
            D.widen ctx invariant_condition joined
          end
        in
        L.dbg "Candidate loop invariant: %a@," (D.fmt ctx) invariant_condition';
        ({ state with
          annotations = IMap.add inv_id invariant_condition' state.annotations },
         true)
      end
    end
    in
    if run_iteration then begin
      L.dbg "Running while loop body: delay remaining: %d@," rem_iterations;
      let (id, state) = execute_control ctx state inv_id bodyc in
      widening_iterations ctx state bodyc inv_id pre_id id (rem_iterations - 1)
    end else
      (state, feedback_id)

  and narrowing_iterations ctx state bodyc inv_id pre_id feedback_id rem_iterations =
    let pre_condition = IMap.find pre_id state.annotations in
    let feedback_condition = IMap.find feedback_id state.annotations in
    let invariant_condition = IMap.find inv_id state.annotations in
    let invariant_condition' = D.join ctx pre_condition feedback_condition in
    let complete = rem_iterations <= 0 || ((D.le ctx invariant_condition' invariant_condition)
                                           && (D.le ctx invariant_condition invariant_condition')) in
    let (state, run_iteration) = if complete then
        (* terminating because analysis of loop complete *)
        (state, false)
      else begin
        let invariant_condition' = D.narrow ctx invariant_condition invariant_condition' in
        (* not terminating because progress was made and iterations remain *)
        ({ state with
          annotations = IMap.add inv_id invariant_condition' state.annotations },
         true)
      end
    in
    if run_iteration then
      let (id, state) = execute_control ctx state inv_id bodyc in
      narrowing_iterations ctx state bodyc inv_id pre_id id (rem_iterations - 1)
    else
      state
        
        

  let execute ctx pgm =
    let env = D.empty_env ctx in
    let env = SMap.fold (fun vname typ env ->
      D.add_variable ctx env vname typ) pgm.variables env in
    let init_state = {
      env = env;
      annotations = IMap.singleton (pgm.init_id) (D.top ctx env);
      vars = pgm.variables
    } in
    let (_id,state) = execute_control ctx init_state pgm.init_id pgm.program in
    state.annotations
      
end

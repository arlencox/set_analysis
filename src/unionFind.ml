module type OrderedType = sig
  type t
  val compare : t -> t -> int
end
module Make(T : OrderedType) =
struct
  module TMap = QUICr.Mapx.Make(T)

  module TSet = QUICr.Setx.Make(T)

  type t = T.t TMap.t

  let empty = TMap.empty

  let add uf v =
    if TMap.mem v uf then
      uf
    else
      TMap.add v v uf

  let add_list uf vl =
    List.fold_left (fun uf v ->
      add uf v
    ) uf vl

  let add_set uf s =
    add_list uf (TSet.elements s)

  let rec get_representative uf v =
    try
      let rep = TMap.find v uf in
      if (T.compare rep v) = 0 then
        (uf,rep)
      else begin
        let (uf, rep) = get_representative uf rep in
        (TMap.add v rep uf, rep)
      end
    with Not_found ->
      (TMap.add v v uf, v)

  let union uf v1 v2 =
    let (uf,v1_rep) = get_representative uf v1 in
    let (uf,v2_rep) = get_representative uf v2 in
    TMap.add v1_rep v2_rep uf

  let rec union_list uf l =
    match l with
      | [] -> uf
      | [v1] -> add uf v1
      | v1::v2::rest ->
          union_list (union uf v1 v2) (v2::rest)

  let union_set uf s =
    union_list uf (TSet.elements s)
    

  let representatives uf =
    let res = ref [] in
    TMap.iter (fun v rep ->
      if (T.compare v rep) = 0 then
        res := rep::!res) uf;
    !res

  let rec add_transitive uf result v =
    let rep = TMap.find v uf in
    Hashtbl.replace result rep ();
    if (T.compare rep v) = 0 then
      ()
    else
      add_transitive uf result rep

  let equivalent uf v =
    let result_hash = Hashtbl.create 1023 in
    Hashtbl.replace result_hash v ();
    add_transitive uf result_hash v;
    let updated = ref true in
    while !updated do
      updated := false;
      TMap.iter (fun v rep ->
        if (Hashtbl.mem result_hash rep) && not (Hashtbl.mem result_hash v) then begin
          Hashtbl.replace result_hash v ();
          updated := true
        end) uf
    done;
    Hashtbl.fold (fun member _unit l -> member::l) result_hash []

  let equivalence_classes uf =
    List.map (equivalent uf) (representatives uf)

  let mem uf v =
    TMap.mem v uf

  let fmt fmt_el ff uf =
    let first = ref true in
    TMap.iter (fun v rep ->
      if !first then
        first := false
      else
        Format.fprintf ff "@ ";
      Format.fprintf ff "%a -> %a" fmt_el v fmt_el rep
    ) uf

  let remove uf v =
    let eqs = equivalence_classes uf in
    let eqs = List.rev_map (fun l -> List.filter (fun el -> el <> v) l) eqs in
    List.fold_left union_list empty eqs


  (* split equivalence class represented by rep so that elements of part become
     a new equivalence class part *must not* contain rep *)
  let split_eq ?fmt:fmt t rep part =
    assert(not (TSet.mem rep part));
    if TSet.is_empty part then
      t
    else begin
      let rep_new = TSet.choose part in
      let equiv = TSet.fold (fun el equiv ->
        TMap.add el rep_new equiv) part t in
      equiv
    end

  let diff_update ?fmt:fmt t p i =
    let n = TSet.diff p i in
    let (t,rep) = get_representative t (TSet.choose p) in
    if TSet.mem rep n then
      (n, split_eq t rep i)
    else
      (n, split_eq t rep n)
        

  (* split equivalence classes so that we have the same partitioning from both eq1 and eq2 *)
  let rec partition ?fmt:(fmt=None) t1 t2 skip2 eq1 eq2 =
    begin match fmt with
      | Some fmt ->
          Format.printf "eq1: %a@."
            (QUICr.Listx.fmt_str_sep " /\\ " (fun ff l ->
              TSet.fmt ~sep:(fun ff -> Format.fprintf ff " = ") fmt ff l)) eq1
      | None -> ()
    end;
    begin match fmt with
      | Some fmt ->
          Format.printf "eq2: %a@."
            (QUICr.Listx.fmt_str_sep " /\\ " (fun ff l ->
              TSet.fmt ~sep:(fun ff -> Format.fprintf ff " = ") fmt ff l)) eq2
      | None -> ()
    end;
    match (eq1, eq2) with
      | (p1::r1, p2::r2) ->
          let inter = TSet.inter p1 p2 in
          if TSet.is_empty inter then
            (* p2 wasn't right, so come back to it later *)
            partition ~fmt:fmt t1 t2 (p2::skip2) eq1 r2
          else
            (* we found a match, do the actual partitioning *)
            let (new1,t1) = diff_update t1 p1 inter in
            let (new2,t2) = diff_update t2 p2 inter in
            let r1 = if TSet.is_empty new1 then r1 else new1::r1 in
            let r2 = if TSet.is_empty new2 then r2 else new2::r2 in
            partition ~fmt:fmt t1 t2 skip2 r1 r2
      | ([], eq2) ->
          assert(eq2 = []);
          (t1, t2)
      | (eq1, []) ->
          (* we ran out of options, try again with the things we skipped from p2 *)
          partition ~fmt:fmt t1 t2 [] eq1 skip2

  module RepMap = QUICr.Mapx.Make(struct
    type t = T.t * T.t
    let compare (a1,a2) (b1,b2) =
      let res = T.compare a1 b1 in
      if res <> 0 then
        res
      else
        T.compare a2 b2 
  end)

  (* partition equivalence classes in t1 to split elements split in t2 *)
  let partition ?fmt:(fmt=None) t1 t2 =
    let rm = TMap.fold2 (fun el _ _ rm ->
      let (_,r1) = get_representative t1 el in
      let (_,r2) = get_representative t2 el in
      try
        let s = RepMap.find (r1,r2) rm in
        RepMap.add (r1,r2) (TSet.add el s) rm
      with Not_found ->
        RepMap.add (r1,r2) (TSet.singleton el) rm
    ) t1 t2 RepMap.empty in
    RepMap.fold (fun _ s res ->
      union_set res s
    ) rm empty
      
    (*let eq1 = equivalence_classes t1 in
    
    let eq2 = equivalence_classes t2 in
    let all2 = List.fold_left List.rev_append [] eq2 in
    List.fold_left 
    let eq1 = equivalence_classes t1 in
    let eq1 = List.rev_map TSet.of_list eq1 in
    let all1 = List.fold_left TSet.union TSet.empty eq1 in*)


    (*
    let s1not2 = TSet.diff all1 all2 in
    let s2not1 = TSet.diff all2 all1 in
    let t1 = add_set t1 s2not1 in
    let t2 = add_set t2 s1not2 in
    let eq1 = equivalence_classes t1 in
    begin match fmt with
      | Some fmt ->
          Format.printf "eq1: %a@."
            (Listx.fmt_str_sep " /\\ " (fun ff l ->
              Listx.fmt_str_sep " = " fmt ff l)) eq1
      | None -> ()
    end;
    let eq1 = List.rev_map TSet.of_list eq1 in
    let eq2 = equivalence_classes t2 in
    begin match fmt with
      | Some fmt ->
          Format.printf "eq2: %a@."
            (Listx.fmt_str_sep " /\\ " (fun ff l ->
              Listx.fmt_str_sep " = " fmt ff l)) eq2
      | None -> ()
    end;
    let eq2 = List.rev_map TSet.of_list eq2 in
    fst (partition ~fmt:fmt t1 t2 [] eq1 eq2)*)

  let unify a b =
    TMap.fold2 (fun key _ _ (a,b) ->
      (add a key, add b key)) a b (a,b)

end

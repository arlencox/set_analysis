TIMEOUT=600

option1=-disable-reductions
option2=-disable-relations

for t in factored_*/*.js0 other_tests/*.js0
do
  echo $t
  time ../Main -timeout $TIMEOUT $t > $t.full.log
  time ../Main -timeout $TIMEOUT $option1 $t > $t.nored.log
  time ../Main -timeout $TIMEOUT $option2 $t > $t.norel.log
done
